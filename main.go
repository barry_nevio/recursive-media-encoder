package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cast"
	"gitlab.com/barry_nevio/goo-tools/Error"
	"gitlab.com/barry_nevio/recursive-media-encoder/models/Settings"
	"gitlab.com/barry_nevio/recursive-media-encoder/models/State"
)

// main ...
func main() {

	var err error

	// Get ffmpeg bin
	ffmpegBin, err := getFfmpegBin()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// Get state
	state, err := State.New()
	if err != nil {
		fmt.Println("ERROR GETTING STATE: " + err.Error())
		return
	}

	// Get settings
	settings, err := Settings.New()
	if err != nil {
		fmt.Println("ERROR GETTING SETTINGS: " + err.Error())
		return
	}

	// Target Files to encode
	var targetFiles []string

	// Walk the path and build slice of files
	err = filepath.Walk(settings.App.RootDir,
		func(path string, info os.FileInfo, err error) error {

			// No dice
			if err != nil {
				return err
			}

			// If it has the target extension and it's not already processed, add it
			if settings.IsTargetExtension(filepath.Ext(path)) && !state.AlreadyEncoded(path) {
				targetFiles = append(targetFiles, path)
			}

			return nil
		})

	if err != nil {
		fmt.Println("ERROR WALKING FILE PATH: " + err.Error())
		return
	}

	// Process files
	fmt.Println("Begining to process " + cast.ToString(len(targetFiles)) + " files\n")
	for _, targetFile := range targetFiles {

		// Start a new encode job for the state
		tStart := time.Now()
		encodeJob := State.EncodeJob{}
		encodeJob.Path = targetFile
		encodeJob.StartTime = tStart.Format("2006-01-02 15:04:05")

		// Get the filename parts on their own
		originalDir, filename := filepath.Split(targetFile)
		currentExt := filepath.Ext(filename)
		filenameNOEXT := strings.TrimSuffix(filename, currentExt)

		// Does file already exist here?
		_, fileStatErr := os.Stat(filepath.FromSlash("./" + filename))
		if !os.IsNotExist(fileStatErr) {
			fmt.Println("File " + filename + " Already exists in this folder, deleting")
			_ = os.Remove(filepath.FromSlash("./" + filename))
			_ = os.Remove(filepath.FromSlash("./" + filenameNOEXT + settings.App.DestinationExtension))
			_ = os.Remove(filepath.FromSlash("./" + filenameNOEXT + "_CONVERTING" + settings.App.DestinationExtension))
		}

		// Copy file to this directory
		fmt.Println("Copying File From Server To This Computer " + filename)
		Copy(targetFile, filepath.FromSlash("./"+filename))
		fmt.Println("Copy Complete")

		// Build ffmaped command
		ffmpegCmd := []string{
			"-i",
			filepath.FromSlash("./" + filename),
		}

		// Add flags from settings to the ffmpeg command
		for _, ffmpegFlag := range settings.FfmpegFlags {
			ffmpegCmd = append(ffmpegCmd, ffmpegFlag)
		}

		// Add the outfile to the command
		ffmpegCmd = append(ffmpegCmd, filepath.FromSlash("./"+filenameNOEXT+"_CONVERTING"+settings.App.DestinationExtension))

		// Run ffmpeg
		fmt.Println("Encoding " + filename + "\n")
		cmd := exec.Command(ffmpegBin, ffmpegCmd...)

		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err := cmd.Run()
		if err != nil {
			fmt.Println("ERROR RUNNING FFMPEG: " + err.Error())
			return
		}

		//** Copy file to destination **//

		// Figure out what destination to use
		toText := "original folder "
		destinationDir := originalDir
		if settings.App.MoveToNewDestination {
			toText = "new destination folder "
			destinationDir = filepath.FromSlash(settings.App.NewDestinationDir)
		}

		// Copy new file backover
		fmt.Println("Copying file back over to " + toText + filename)
		finalDest := destinationDir + filenameNOEXT + settings.App.DestinationExtension
		// Does the file we want to move already exist on the destination?
		_, destFileErr := os.Stat(finalDest)
		if !os.IsNotExist(destFileErr) {
			fmt.Println("File already exists, adding uniqueness...")
			uuid4, _ := uuid.NewV4()
			finalDest = filepath.FromSlash(destinationDir + filenameNOEXT + "_" + uuid4.String() + settings.App.DestinationExtension)
		}

		Copy(filepath.FromSlash("./"+filenameNOEXT+"_CONVERTING"+settings.App.DestinationExtension), finalDest)

		// Cleaning up.. Delete files
		fmt.Println("Cleaning up... ")
		_ = os.Remove("./" + filename)
		_ = os.Remove("./" + filenameNOEXT + "_CONVERTING" + settings.App.DestinationExtension)

		// Delete Original File?
		if settings.App.DeleteOriginalFile {
			_ = os.Remove(targetFile)
		}

		// Done
		tFinish := time.Now()
		encodeJob.FinishTime = tFinish.Format("2006-01-02 15:04:05")
		state.AddCompletedEncodeJob(encodeJob)
		fmt.Println("Successfully Completed Processing " + filename)

	}

}

// getFfmpegBin ...
func getFfmpegBin() (string, error) {

	// Current dir
	dir, _ := os.Getwd()

	// Set the ffmpeg bins according to the OS
	switch strings.ToUpper(runtime.GOOS) {
	case "WINDOWS":
		return filepath.FromSlash(dir + "/ffmpeg/4.2.2/windows/bin/ffmpeg.exe"), nil
		//ffmpegConfig.FfprobeBin = dir + "/ffmpeg/4.1.4/windows/bin/ffprobe.exe"
	case "LINUX":
		return filepath.FromSlash(dir + "/ffmpeg/4.1.4/linux/ffmpeg"), nil
		//ffmpegConfig.FfprobeBin = dir + "/ffmpeg/4.1.4/linux/ffprobe"
	case "DARWIN":
		return filepath.FromSlash(dir + "/ffmpeg/4.2.1/macos/bin/ffmpeg"), nil
		//ffmpegConfig.FfprobeBin = dir + "/ffmpeg/4.1.4/macos/bin/ffprobe"
	case "FREEBSD":
		// TODO: Compile a freebsd ffmpeg executeable
		return "ffmpeg", nil
		//ffmpegConfig.FfprobeBin = dir + "/ffmpeg/4.1.4/macos/bin/ffprobe"
	default:
		return "", Error.New("CAN NOT DETECT YOUR OS")
	}

}

// Copy ...
func Copy(src, dst string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}
	return out.Close()
}
