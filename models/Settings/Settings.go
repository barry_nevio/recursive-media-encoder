package Settings

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
)

type Context struct {
	App struct {
		RootDir              string   `json:"root_dir"`
		TargetExtensions     []string `json:"target_extensions"`
		DestinationExtension string   `json:"destination_extension"`
		MoveToNewDestination bool     `json:"move_to_new_destination"`
		NewDestinationDir    string   `json:"new_desitination_dir"`
		DeleteOriginalFile   bool     `json:"delete_original_file"`
	} `json:"app"`
	FfmpegFlags []string `json:"ffmpeg_flags"`
}

func New() (Context, error) {
	c := Context{}

	// Open Settings File
	settingsReader, openSettingsErr := os.Open("./settings.json")
	if openSettingsErr != nil {
		return c, openSettingsErr
	}

	// Read settings file
	settingsBytes, settingsReadErr := ioutil.ReadAll(settingsReader)
	if settingsReadErr != nil {
		return c, settingsReadErr
	}

	// Marshal
	_ = json.Unmarshal(settingsBytes, &c)

	return c, nil
}

func (c *Context) IsTargetExtension(ext string) bool {
	for _, targetExt := range c.App.TargetExtensions {
		if strings.ToLower(ext) == targetExt {
			return true
		}
	}
	return false
}
