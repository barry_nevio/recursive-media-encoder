package State

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"os"
)

type EncodeJob struct {
	Path       string `json:"string"`
	StartTime  string `json:"start_time"`
	FinishTime string `json:"finish_time"`
}

type Context struct {
	CompletedEncodeJobs []EncodeJob `json:"completed_encode_jobs"`
}

func New() (Context, error) {
	c := Context{}

	// Read state File
	stateReader, openstateErr := os.Open("./state.json")
	if openstateErr != nil {
		return c, openstateErr
	}

	// Read state file
	stateBytes, stateReadErr := ioutil.ReadAll(stateReader)
	if stateReadErr != nil {
		return c, stateReadErr
	}

	// Marshal
	_ = json.Unmarshal(stateBytes, &c)

	return c, nil
}

func (c *Context) UpdateFile() error {

	var err error

	// Marshal the struct
	currentState, err := json.Marshal(c)
	if err != nil {
		return err
	}

	// pretty it
	var out bytes.Buffer
	_ = json.Indent(&out, currentState, "", "  ")

	// Overwrite the file
	err = ioutil.WriteFile("./state.json", out.Bytes(), 0644)
	if err != nil {
		return err
	}

	return nil
}

func (c *Context) AddCompletedEncodeJob(encodeJob EncodeJob) {

	c.CompletedEncodeJobs = append(c.CompletedEncodeJobs, encodeJob)
	c.UpdateFile()

}

// AlreadyEncoded ... checks the stste if a file was already encoded
func (c *Context) AlreadyEncoded(path string) bool {

	for _, encodedFile := range c.CompletedEncodeJobs {
		if encodedFile.Path == path {
			return true
		}
	}
	return false
}
